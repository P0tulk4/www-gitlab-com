---
layout: markdown_page
title: "Category Direction - Continuous Integration Scaling"
description: "Lean more about how we are scaling Continuous Integration on GitLab.com!"
canonical_path: "/direction/verify/continuous_integration_scaling/"
---

- TOC
{:toc}

## Continuous Integration Scaling 

In FY22, we have committed to [SaaS First](/direction/enablement/#saas-first). In the Verify Stage, this means prioritizing the scale of Continuous Integration and ensuring our users on GitLab.com are leveraging a reliable and available service. We are focused on a goal of 20M builds per day as it represents not only a target to drive our future architecture, but a volume that we expect to achieve within several quarters.

### Investment Allocation 

Currently, we are staffing this Category with 10% of the Verify output, or equivalent to ~3 engineers in Verify. After we establish the load testing effort, we will evaluate if additional headcount will be required to support this effort. 

We are expecting to on-ramp additional headcount in FY22-Q3 per [product#2178](https://gitlab.com/gitlab-com/Product/-/issues/2178), raising the percentage to ~24% of the Verify headcount. 

## Additional Resources

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=ci%3A%3Ascaling)
- [Overall Vision of the Verify stage](/direction/ops/#verify)

For specific information and features related to authoring/defining pipelines, check out [Pipeline Authoring](/direction/verify/pipeline_authoring). You may also be looking for one of the following related product direction pages: [GitLab Runner](/direction/verify/runner/).

## What's Next & Why

To begin working towards our goal, we will focus on building load testing environments in which we can perform measured tests around the initial primary architecture changes listed below:

1. [Define Scope of CI Scaling](https://gitlab.com/groups/gitlab-org/-/epics/5745)

## Architectural Overview 

The Next Generation of CI/CD Scale as been explored in our [architecture blueprint](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/)). Some of the findings are applied here to confront the challenges of CI scale. 

### Challenges 

1. [Primary Key Storage](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/#we-are-running-out-of-the-capacity-to-store-primary-keys)
1. [ci_builds table size](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/#the-table-is-too-large) 
1. [Queuing mechanisms](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/#queuing-mechanisms-are-using-the-large-table)
1. [Large amounts of data](https://docs.gitlab.com/ee/architecture/blueprints/ci_scale/#moving-big-amounts-of-data-is-challenging)

###  Vision Items 

Our top vision items we have defined include: 

1. [Improve GitLab CI/CD data model](https://gitlab.com/gitlab-org/architecture/tasks/-/issues/5)
1. [Improve Runner job queuing](https://gitlab.com/gitlab-org/gitlab/-/issues/322972)
1. [GitLab Runner Autoscaler architecture](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/57051)






